<?php

class AddressController extends Controller
{
  private $checkoutDao;

  public function __construct()
  {
    $this->view = new AddressView();

    $this->addressDao = new AddressDao();
  }
  public function indexAction()
  {
    return;
  }
  public function addAction()
  {

    $message = Message::singleton();

    $viewModel = array();

    $id =  array_key_exists ('id', $_REQUEST) ? $_REQUEST['id'] : 0;

    $checkout = array_key_exists ('checkout', $_REQUEST) ? $_REQUEST['checkout'] : 0;

   if(array_key_exists ('save', $_REQUEST))
   {
     $user_id =  array_key_exists ('id', $_REQUEST) ? $_REQUEST['id'] : 0;
     $street =  array_key_exists ('street', $_REQUEST) ? $_REQUEST['street'] : '';
     $number = array_key_exists ('number', $_REQUEST) ? $_REQUEST['number'] : 0;
     $cep = array_key_exists ('cep', $_REQUEST) ? $_REQUEST['cep'] : '';
     $city = array_key_exists ('city', $_REQUEST) ? $_REQUEST['city'] : '';
     $state = array_key_exists ('state', $_REQUEST) ? $_REQUEST['state'] : '';
     $district = array_key_exists ('district', $_REQUEST) ? $_REQUEST['district'] : '';

     try
     {
       $address = new Address();
       $address->setUserId($user_id);
       $address->setStreet($street);
       $address->setNumber($number);
       $address->setCep($cep);
       $address->setCity($city);
       $address->setState($state);
       $address->setDistrict($district);

       $addressDao = new AddressDao();

       if($addressDao->insert($address))
         $message->addMessage('Adicionado com sucesso!');

       $viewModel = array(
         'addresses' => $addressDao->getAll($id),
       );

       $this->view = new CheckoutView();

       $this->setRoute($this->view->getCheckoutRoute());

     }
     catch(Exception $e)
     {
       $message->addWarning($e->getMessage());
     }
   }
   else
   {
     $addressDao = new AddressDao();

     $viewModel = array(
       'addresses' => '$addressDao->getAll($id)',
     );

     $this->setRoute($this->view->getAddRoute());

   }

   $this->showView($viewModel);
  }

  public function listAction()
  {
    $id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;

    $viewModel = array(
        'addresses' => $this->addressDao->getAll($id),
      );

    $this->setRoute($this->view->getListRoute());

    $this->showView($viewModel);

    return;
  }

  public function editAction()
  {
      $address_id = array_key_exists ('addressid', $_REQUEST) ? $_REQUEST['addressid'] : 0;

      $message = Message::singleton();

  		if(array_key_exists ('save', $_REQUEST))
  		{
  			$id_user =  array_key_exists ('id', $_REQUEST) ? $_REQUEST['id'] : 0;
  			$street =  array_key_exists ('street', $_REQUEST) ? $_REQUEST['street'] : '';
        printf($street);
  			$number = array_key_exists ('number', $_REQUEST) ? $_REQUEST['number'] : 0;
        $cep = array_key_exists ('cep', $_REQUEST) ? $_REQUEST['cep'] : '';
        $city = array_key_exists ('city', $_REQUEST) ? $_REQUEST['city'] : '';
        $state = array_key_exists ('state', $_REQUEST) ? $_REQUEST['state'] : '';
        $district = array_key_exists ('district', $_REQUEST) ? $_REQUEST['district'] : '';

  			try
  			{
  				$address = new Address();
          $address->setId($address_id);
          $address->setUserId($id_user);
          $address->setStreet($street);
          $address->setNumber($number);
          $address->setCep($cep);
          $address->setCity($city);
          $address->setState($state);
          $address->setDistrict($district);

  				$addressDao = new AddressDao();

  				if($addressDao->update($address))
  					$message->addMessage('Editado com sucesso!');

  				$viewModel = array(
  					'addresses' => $addressDao->getAll($id_user),
  				);

          $this->view = new CheckoutView();

          $this->setRoute($this->view->getCheckoutRoute());
  			}
  			catch(Exception $e)
  			{
  				$message->addWarning($e->getMessage());
  			}
  		}
  		else
  		{
  			$addressDao = new AddressDao();

  			$viewModel = array(
  				'address' => $addressDao->getAddress($address_id),
  			);

  			$this->setRoute($this->view->getEditRoute());
  		}

  		$this->showView($viewModel);
  }

  public function deleteAction()
  {
    $message = Message::singleton();

    $id =  array_key_exists ('id', $_REQUEST) ? $_REQUEST['id'] : '';

    $address_id = array_key_exists ('addressid', $_REQUEST) ? $_REQUEST['addressid'] : '';

    $addressDao = new AddressDao();

		$addressDao->delete($address_id);

		$viewModel = array(
			'addresses' => $addressDao->getAll($id),
		);

		$message->addMessage('Endereço removido com sucesso!');

    $this->view = new CheckoutView();

    $this->setRoute($this->view->getCheckoutRoute());

		$this->showView($viewModel);

		return;
  }

  public function selectAction()
  {
    $message = Message::singleton();

		$address = AddressActive::load();

		$id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;

		$address_id = array_key_exists ('addressid', $_GET) ? $_GET['addressid'] : 0;

		$address1 = $this->addressDao->getAddress($address_id);

		AddressActive::select($address1);

		AddressActive::save();

    $this->view = new CheckoutView();

    $cep =  $address1->getCep();

		$cep = trim($cep);
 		$cep = str_replace("-", "", $cep);

		$_args = array(
					 'nCdServico' => '40010',
						'sCepOrigem'=>'79600080', 'sCepDestino' => $cep,
					 'nVlPeso' => '1', 'nVlComprimento' => 30,
					 'nVlAltura' => 15, 'nVlLargura' => 20  );

		$calculafrete = new CalculaFrete( $_args );

		$viewModel = array(
			'addresses' => $this->addressDao->getAll($id),
      'valorFrete' => $calculafrete->request(),
		);

		$this->setRoute($this->view->getCheckoutRoute());

		$this->showView($viewModel);
	}

}
