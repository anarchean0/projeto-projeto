<?php

class UserController extends Controller {
  private $view;
  private $userDao;
  private $addressDao;

  public function __construct() {
    $this->view = new UserView();
    $this->userDao = new UserDao();
    $this->addressDao = new AddressDao();
  }

  private function extractUser($data) {
    $user = new User();
    $user->setName($data["name"]);
    $user->setEmail($data["email"]);
    $user->setPassword($data["password"]);
    $user->setPhone($data["phone"]);
    $user->setNotifyPromotions(array_key_exists("notify-promotions", $data));
    $user->setNewsletter(array_key_exists("newsletter", $data));
    $user->setCpf($data["cpf"]);

    return $user;
  }

  private function extractAddress($prefix, $data) {
    $address = new Address();

    $address->setCep($data["{$prefix}cep"]);
    $address->setStreet($data["{$prefix}street"]);
    $address->setNumber($data["{$prefix}number"]);
    $address->setDistrict($data["{$prefix}district"]);
    $address->setCity($data["{$prefix}city"]);
    $address->setState($data["{$prefix}state"]);

    return $address;
  }

  public function registerAction() {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $user = $this->extractUser($_POST);
      $address = $this->extractAddress("address-", $_POST);

      $user = $this->userDao->save($user);

      if (!$user) {
        echo "Can't save user";
        return;
      }

      $address->setUserId($user->getId());
      $this->addressDao->insert($address);

      $this->makeActivation($user);

      $this->setRoute($this->view->getSucessfulRegistrationRoute());
      $this->showView(array('user' => $user));
    } else {
      $viewModel = array();

      $this->setRoute($this->view->getRegisterRoute());
      $this->showView($viewModel);
    }
  }

  public function verifyAction() {
    $user_id = $_GET["user"];
    $validity = $_GET["validity"];
    $verification = $_GET["verification"];

    $user = $this->userDao->getUserById($user_id);

    if ($user && $user->isActive()) {
      header('Location: http://' . $_SERVER['SERVER_NAME'] . ':'
          . $_SERVER['SERVER_PORT'] . '/');
      return;
    }

    $params = "user=$user_id&validity=$validity";

    $validity_dt = new DateTime();
    $validity_dt->setTimestamp((int) $validity);

    if ($validity_dt->getTimestamp() <= (new DateTime())->getTimestamp()) {
      echo "This token has expired, you can register again if you still want "
          . "to use this website";

      if ($user) {
        $this->userDao->remove($user);
      }
      return;
    }

    $digest = hash_hmac('sha384', $params, APPLICATION_CONFIG["key"]);

    if (!hash_equals($digest, $verification)) {
      echo "Invalid verification token";
      return;
    }

    if (!$user) {
      echo "User no longer exists";
      return;
    }

    $user->setActive(true);
    $this->userDao->save($user);

    UserLoginManager::getInstance()->setUser($user);
    $this->setRoute($this->view->getActivateRoute());
    $this->showView(array("user" => $user));
  }

  private function makeActivation($user) {
    $validity = new DateTime();
    $validity->add(new DateInterval("P1W"));

    $mailer = new Mailer();
    $host = $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];

    $verify_params = "user={$user->getId()}&validity={$validity->getTimestamp()}";

    $digest = hash_hmac('sha384', $verify_params, APPLICATION_CONFIG["key"]);

    $mailer->sendEmail($user->getEmail(), "Projeto Projeto User Verification",
      "To activate your use access http://$host/?controller=User&"
       . "action=verify&$verify_params&verification=$digest");
  }

  public function loginAction() {
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
      $this->setRoute($this->view->getLoginRoute());
      $this->showView(array('errors' => null, 'email' => null));
    } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $email = $_POST['email'];
      $password = $_POST['password'];

      $user = $this->userDao->getUserByEmail($email);

      if (!$user || !$user->isPassword($password)) {
        $this->setRoute($this->view->getLoginRoute());
        $this->showView(array(
          'errors' => array(
            'Usuário com essa combinação de usuário e senha não existe'
          ),
          'email' => $email
        ));
        return;
      }

      if ($user->shouldRehash()) {
        $user->setPassword($password);
        $this->userDao->save($user);
      }

      UserLoginManager::getInstance()->setUser($user);

      header('Location: http://' . $_SERVER['SERVER_NAME'] . ':'
          . $_SERVER['SERVER_PORT'] . '/');
    }
  }

  public function logoutAction() {
    UserLoginManager::getInstance()->unsetUser();

    header('Location: http://' . $_SERVER['SERVER_NAME'] . ':'
        . $_SERVER['SERVER_PORT'] . '/');
  }

  public function resetPasswordAction() {
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
      $this->setRoute($this->view->getResetPasswordRoute());
      $this->showView(array());
    } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $email = $_POST['email'];
      $user = $this->userDao->getUserByEmail($email);

      if ($user) {
        $this->sendReset($user);
      }

      $this->setRoute($this->view->getResetSentRoute());
      $this->showView(array());
    }
  }

  private function sendReset($user) {
    $validity = new DateTime();
    $validity->add(new DateInterval('P1W'));
    $validity = $validity->getTimestamp();

    $params = "user=" . $user->getId() . "&validity=$validity";
    $digest = hash_hmac('sha384', $params, APPLICATION_CONFIG['key']);

    $url = 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']
        . "/?controller=User&action=setPassword&$params"
        . "&verification=$digest";

    $mailer = new Mailer();
    $mailer->sendEmail($user->getEmail(), 'Pedido de troca de senha',
        'Para trocar sua senha clique em: ' . $url);
  }

  public function setPasswordAction() {
    $user_id = $_REQUEST['user'];
    $validity = $_REQUEST['validity'];

    $params = "user=$user_id&validity=$validity";

    $digest = hash_hmac('sha384', $params, APPLICATION_CONFIG['key']);
    $validity = (int) $validity;
    $user = $this->userDao->getUserById($user_id);

    if (!hash_equals($digest, $_REQUEST['verification'])
        || $validity < (new DateTime())->getTimestamp()
        || !$user) {
      $this->setRoute($this->view->getInvalidPasswordResetRoute());
      $this->showView(array());
      die();
    }

    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
      $this->setRoute($this->view->getSetPasswordRoute());
      $this->showView(array(
        'user_id' => $user_id,
        'validity' => $validity,
        'digest' => $digest,
        'errors' => array()
      ));
    } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $password = $_REQUEST['password'];
      $confirm_password = $_REQUEST['confirm-password'];

      if ($password != $confirm_password) {
        $this->setRoute($this->view->getSetPasswordRoute());
        $this->showView(array(
          'user_id' => $user_id,
          'validity' => $validity,
          'digest' => $digest,
          'errors' => array(
            'Senha e confirmação não são iguais'
          )
        ));
        die();
      }

      $user->setPassword($password);
      $this->userDao->save($user);

      header('Location: http://' . $_SERVER['SERVER_NAME'] . ':'
          . $_SERVER['SERVER_PORT'] . '/?controller=User&action=login');
    }
  }

  public function updateAction() {
    $user = UserLoginManager::getInstance()->getUser();

    if (!$user || !$user->isActive()) {
      Message::singleton()->addMessage("Você precisa estar logado para realizar esta operação");
      header('Location: /');
      return;
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $post_user = $this->extractUser($_POST);
      $user->setName($post_user->getName());
      $user->setEmail($post_user->getEmail());
      $user->setPhone($post_user->getPhone());
      $user->setNotifyPromotions($post_user->getNotifyPromotions());
      $user->setNewsletter($post_user->getNewsletter());
      $user->setCpf($post_user->getCpf());

      $this->userDao->save($user);

      Message::singleton()->addMessage("Usuário modificado com sucesso!");
      header('Location: /');
    } else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
      $this->setRoute($this->view->getUpdateUserRoute());
      $this->showView(array(
        'user' => $user
      ));
    }
  }

  public function deactivateAction() {
    $user = UserLoginManager::getInstance()->getUser();

    if (!$user) {
      Message::singleton()->addMessage("Você precisa estar logado para executar esta operação");
      header('Location: /');
      return;
    }

    $user->setActive(false);
    $this->userDao->save($user);

    UserLoginManager::getInstance()->unsetUser();
    header('Location: /');
  }
}
