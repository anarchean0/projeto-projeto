<?php

class CheckoutController extends Controller
{
  private $checkoutDao;

  public function __construct()
  {
    $this->view = new CheckoutView();

    $this->checkoutDao = new CheckoutDao();

    $this->addressDao = new AddressDao();
  }
  public function indexAction()
  {
    return;
  }
  public function createAction()
  {
    $message = Message::singleton();

    $viewModel = array();


    if(array_key_exists ('save', $_POST))
    {

      $paymentMethod =  array_key_exists ('paymentMethod', $_POST) ? $_POST['paymentMethod'] : '';

      try
      {
        if(empty($paymentMethod))
          throw new Exception('Preencha a forma de pagamento.');

        $checkout = new Checkout();

        if($this->checkoutDao->insert($chekout))
          $message->addMessage('Checkout realizado com sucesso!');
        else
          throw new Exception('Problema ao realizar o checkout.');
      }
      catch(Exception $e)
      {
        $this->setRoute($this->view->getCreateRoute());

        $message->addWarning($e->getMessage());
      }
    }
    else
      $this->setRoute($this->view->getCreateRoute());


    $this->showView($viewModel);

    return;
  }

  public function listAction()
  {
    $viewModel = array(
        'checkouts' => $this->checkoutDao->getAll(),
      );

    $this->setRoute($this->view->getListRoute());

    $this->showView($viewModel);

    return;
  }
  public function checkoutAction()
  {
      $id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;

      $viewModel = array(
        'addresses' => $this->addressDao->getAll($id),
      );

    $this->setRoute($this->view->getCheckoutRoute());

    $this->showView($viewModel);

    return;
  }
  public function paymentAction()
	{
    $messages = array();
    $warnings = array ();
    $message = Message::singleton();

		if(isset ($_POST['data']))
		{
			$success = FALSE;

			$data = $_POST['data'];

		    $aux = array (
									'name'						=> 'Nome',
									'email'						=> 'Email',
									'cardNumber'				=> 'Número do cartão',
									'cardExpirationMonth'		=> 'Mês de validade do cartão.',
									'cardExpirationYear'		=> 'Ano de validade do cartão',
									'cardCvv'					=> 'Código de Segurança',
									'creditCardHolderBirthDate'	=> 'Data de Nascimento',
									'creditCardHolderName'		=> 'Nome',
									'creditCardHolderCPF'		=> 'CPF',
									'creditCardHolderAreaCode'	=> 'DDD',
									'creditCardHolderPhone'		=> 'Telefone',
									'billingAddressPostalCode'	=> 'CEP',
									'billingAddressStreet'		=> 'Endereço',
									'billingAddressNumber'		=> 'Número',
									'billingAddressDistrict'	=> 'Bairro',
									'billingAddressCity'		=> 'Cidade',
									'billingAddressState'		=> 'Estado',
									'billingAddressCountry'		=>'Pais');

					foreach ($aux as $key => $value)
						if($value != '')
							if (!array_key_exists ($key, $data) || trim ($data [$key]) == '')
								$warnings [] = 'Preencha corretamente o campo <b>[ '.$value.' ]</b>';

					if (!sizeof ($warnings))
					{
						include('checkout/usingCreditCard.php');
            ShoppingCart::clear();

            $this->setRoute($this->view->getCompleteRoute());

            $this->showView();

					}else{
             $message->addMessage('Ocorreu um erro ao processar sua compra');

             //mudar aqui
            $viewModel = array(
              'addresses' => $this->addressDao->getAll('1'),
            );
            $this->setRoute($this->view->getCheckoutRoute());

            $this->showView($viewModel);
          }



		}
		else
		{

		}
	}
}
