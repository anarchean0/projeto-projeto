<?php

class ItemStoreController extends Controller
{
	private $itemStoreDao;

	public function __construct()
	{
		$this->view = new ItemStoreView();

		$this->itemStoreDao = new ItemStoreDao();


	}
	public function indexAction()
	{
		return;
	}

	public function addAction()
	{

		$message = Message::singleton();

		$shoppingCart = ShoppingCart::load();

		$id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;
		$quantity = array_key_exists ('quantity', $_GET) ? $_GET['quantity'] : 1;


		$itemStore = $this->itemStoreDao->getItemStore($id);

		ShoppingCart::add($itemStore, $quantity);

		ShoppingCart::save();

		$message->addMessage($itemStore->getName() . ' foi adicionado ao carrinho!');

		$viewModel = array(
			'itens' => $this->itemStoreDao->getAll(),
		);

		$this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);
	}

	public function listAction()
	{
		$viewModel = array(
			'itens' => $this->itemStoreDao->getAll()
		);

		$this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);

		return ;
	}

	public function viewAction()
	{
		$id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;

		$viewModel = array(
			'item' => $this->itemStoreDao->getItemStore($id),
		);

		$this->setRoute(ItemStoreView::detailsRoute);

		$this->showView($viewModel);

		return;
	}
}
