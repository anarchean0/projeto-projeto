<?php

class CheckoutView
{
	public function __contruct(){}

	public function getIndexRoute()
	{
		return 'checkout/index.php';
	}

	public function getListRoute()
	{
		return 'checkout/list.php';
	}

	public function getCreateRoute()
	{
		return 'checkout/create.php';
	}

	public function getCompleteRoute()
	{
		return 'checkout/listCheckoutComplete.php';
	}

	public function getCheckoutRoute()
	{
		return 'checkout/checkout.php';
	}
}
