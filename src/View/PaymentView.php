<?php

class PaymentView
{
	public function __contruct(){}
	
	public function getIndexRoute()
	{
		return 'payment/index.php';
	}
	
	public function getListRoute()
	{
		return 'payment/list.php';
	}

	public function getCreateRoute()
	{
		return 'payment/create.php';
	}
}