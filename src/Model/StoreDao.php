<?php

class ItemStoreDao
{
  const _table = 'item_store';

  public function __construct() { }

  public function getAll()
  {
    
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table;
    
    $sth = $db->prepare($sql);

    $sth->execute();

    $users = array();
    
    while($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
      $itemStore = new ItemStore();
      $itemStore->setId($obj->id);
      $itemStore->setName($obj->name);
      $itemStore->setDescription($obj->description);
      $itemStore->setPrice($obj->price);
     
      $itens[] = $itemStore; 
    }
    
    return $itens;  
  }


  public function getItemStore($id)
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table . ' WHERE id = ?';

    $sth = $db->prepare($sql);

    $sth->bindValue(1, $id, PDO::PARAM_STR);

    $sth->execute();

    if($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
        $itemStore = new ItemStore();
        $itemStore->setId($obj->id);
        $itemStore->setName($obj->name);
        $itemStore->setDescription($obj->description);
        $itemStore->setPrice($obj->price);
    
        return $itemStore;
    }

    return false;
  }
}