<?php

class ItemStoreDao
{
  const _table = 'item_store';

  public function __construct() { }

  public function getAll()
  {

    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table;

    $sth = $db->prepare($sql);

    $sth->execute();

    $itens = array();

    while($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
      $itemStore = new ItemStore();
      $itemStore->setId($obj->id);
      $itemStore->setName($obj->name);
      $itemStore->setDescription($obj->description);
      $itemStore->setPrice($obj->price);
	    $itemStore->setPicture($obj->picture);
      $itemStore->setQuantity($obj->quatity);

      $itens[] = $itemStore;
    }

    return $itens;
  }


  public function getItemStore($id)
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table . ' WHERE id = ?';

    $sth = $db->prepare($sql);

    $sth->bindValue(1, $id, PDO::PARAM_STR);

    $sth->execute();

    if($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
        $itemStore = new ItemStore();
        $itemStore->setId($obj->id);
        $itemStore->setName($obj->name);
        $itemStore->setDescription($obj->description);
        $itemStore->setPrice($obj->price);
		    $itemStore->setPicture($obj->picture);
        $itemStore->setQuantity($obj->quatity);

        return $itemStore;
    }

    return false;
  }

  public function insert($itemStore)
	{
		$db = Database::singleton();

		$message = Message::singleton();

		try{

			$db->beginTransaction();

			$sql = "INSERT INTO item_store (name, description, price, picture, category_id, quatity) VALUES (?, ?, ?, ?, ?, ?)";

			$sth = $db->prepare($sql);
			$sth->bindValue(1, $itemStore->getName(), PDO::PARAM_STR);
			$sth->bindValue(2, $itemStore->getDescription(), PDO::PARAM_STR);
			$sth->bindValue(3, $itemStore->getPrice(), PDO::PARAM_INT);
			$sth->bindValue(4, $itemStore->getPicture(), PDO::PARAM_STR);
			$sth->bindValue(5, $itemStore->getCategoryId(), PDO::PARAM_INT);
      $sth->bindValue(6, $itemStore->getQuantity(), PDO::PARAM_INT);

			if(!$sth->execute())
				return true;

			$db->commit();
		}
		catch(PDOException $e)
		{
			$db->rollBack();
			$message->addWarning($e->getMessage());
		}

		return false;
	}

  public function delete($id)
	{
    $db = Database::singleton();

    $message = Message::singleton();

    try{

      $db->beginTransaction();

      $sql = "DELETE FROM item_store where id = ?";

      $sth = $db->prepare($sql);

      $sth->bindValue(1, $id, PDO::PARAM_INT);

      if(!$sth->execute())
		    return true;

      $db->commit();
    }
    catch(PDOException $e)
    {
      $db->rollBack();
      $message->addWarning($e->getMessage());
    }

    return false;
  }

  public function update($itemStore)
	{
		$db = Database::singleton();

		$message = Message::singleton();

		try{
			$db->beginTransaction();
			$sql = 'UPDATE item_store SET name = ?, description = ?, price = ?, picture = ?, category_id = ?, quatity = ?  WHERE id = ?';

			$sth = $db->prepare($sql);

      $sth->bindValue(1, $itemStore->getName(), PDO::PARAM_STR);
			$sth->bindValue(2, $itemStore->getDescription(), PDO::PARAM_STR);
			$sth->bindValue(3, $itemStore->getPrice(), PDO::PARAM_INT);
			$sth->bindValue(4, $itemStore->getPicture(), PDO::PARAM_STR);
			$sth->bindValue(5, $itemStore->getCategoryId(), PDO::PARAM_INT);
      $sth->bindValue(6, $itemStore->getQuantity(), PDO::PARAM_INT);
      $sth->bindValue(7, $itemStore->getId(), PDO::PARAM_INT);

			if(!$sth->execute())
				return true;

			$db->commit();
		}
		catch(PDOException $e)
		{
      $db->rollBack();
			$message->addWarning($e->getMessage());
		}

		return false;
	}
}
