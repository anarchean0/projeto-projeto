<?php

class ProductDao
{
  const _table = 'item_store';

  public function __construct() { }

  public function getAll()
  {

    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table;

    $sth = $db->prepare($sql);

    $sth->execute();

    $products = array();

    while($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
      $product = new Product();
      $product->setId($obj->id);
      $product->setName($obj->name);
      $product->setDescription($obj->description);
      $product->setPrice($obj->price);
      $product->setPicture($obj->picture);
      $product->setCategory($obj->category_id);
      $product->setQuatity($obj->quatity);

      $products[] = $product;
    }

    return $products;
  }

  public function getCategories()
  {
    $db = Database::singleton();

    $sql = 'SELECT DISTINCT category_id FROM ' . self::_table;

    $sth = $db->prepare($sql);

    $sth->execute();

    $users = array();

    while($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
      $category = $obj->category_id;
      $categories[] = $category;
    }

    return $categories;

  }

  public function getSearch($category, $name)
  {
    if($name == -1){
        $db = Database::singleton();

        $sql = 'SELECT * FROM ' . self::_table . ' WHERE category_id = ?';

        $sth = $db->prepare($sql);
        $sth->bindValue(1, $category, PDO::PARAM_STR);

        $sth->execute();

        $products = array();

        while($obj = $sth->fetch(PDO::FETCH_OBJ))
        {
          $product = new Product();
          $product->setId($obj->id);
          $product->setName($obj->name);
          $product->setDescription($obj->description);
          $product->setPrice($obj->price);
          $product->setPicture($obj->picture);
          $product->setCategory($obj->category_id);
          $product->setQuatity($obj->quatity);

          $products[] = $product;
        }

        return $products;
      }else if($category == -1){
        $db = Database::singleton();

        $sql = 'SELECT * FROM ' . self::_table . ' WHERE name ~ :1';

        $sth = $db->prepare($sql);
        $sth->bindValue('1', $name, PDO::PARAM_STR);

        $sth->execute();

        $products = array();

        while($obj = $sth->fetch(PDO::FETCH_OBJ))
        {
          $product = new Product();
          $product->setId($obj->id);
          $product->setName($obj->name);
          $product->setDescription($obj->description);
          $product->setPrice($obj->price);
          $product->setPicture($obj->picture);
          $product->setCategory($obj->category_id);
          $product->setQuatity($obj->quatity);

          $products[] = $product;
        }
        return $products;
      }else{
          $db = Database::singleton();

          $sql = 'SELECT * FROM ' . self::_table . ' WHERE name ~ :1 and category_id = :2';

          $sth = $db->prepare($sql);
          $sth->bindValue('1', $name, PDO::PARAM_STR);
          $sth->bindValue(2, $category, PDO::PARAM_STR);

          $sth->execute();

          $products = array();

          while($obj = $sth->fetch(PDO::FETCH_OBJ))
          {
            $product = new Product();
            $product->setId($obj->id);
            $product->setName($obj->name);
            $product->setDescription($obj->description);
            $product->setPrice($obj->price);
            $product->setPicture($obj->picture);
            $product->setCategory($obj->category_id);
            $product->setQuatity($obj->quatity);

            $products[] = $product;
        }
        return $products;

      }
  }

  public function getProduct($id)
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table . ' WHERE id = ?';

    $sth = $db->prepare($sql);

    $sth->bindValue(1, $id, PDO::PARAM_STR);

    $sth->execute();

    if($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
        $product = new Products();
        $product->setId($obj->id);
        $product->setName($obj->name);
        $product->setDescription($obj->description);
        $product->setPrice($obj->price);
        $product->setPicture($obj->picture);
        $product->setCategory($obj->category_id);
        $product->setQuatity($obj->quatity);

        return $product;
    }

    return false;
  }
}
