<?php

class UserDao {
  const _table = 'users';

  public function getUserByEmail($email) {
    $db = Database::singleton();

    $sql = "SELECT * FROM " . self::_table . " WHERE email = :email";
    $stmt = $db->prepare($sql);

    $stmt->execute(array(
      ":email" => $email
    ));

    $row = $stmt->fetch();

    if ($row) {
      return $this->loadUser($row);
    }

    return null;
  }

  public function getUserById($id) {
    $db = Database::singleton();

    $sql = "SELECT * FROM " . self::_table . " WHERE id = :id";
    $stmt = $db->prepare($sql);

    $stmt->execute(array(
      ":id" => $id
    ));

    $row = $stmt->fetch();

    if ($row) {
      return $this->loadUser($row);
    }

    return null;
  }

  public function save($user) {
    if (!$user->getId()) {
      return $this->insert($user);
    } else {
      return $this->update($user);
    }
  }

  private function insert($user) {
    $db = Database::singleton();

    $sql = "INSERT INTO " . self::_table . "(name, email, "
        . "password_hash, phone, notify_sales, newsletter, cpf, active, staff) "
        . "VALUES (:name, :email, :password_hash, :phone, :notify_promotions, "
        . ":newsletter, :cpf, :active, :staff)";

    $stmt = $db->prepare($sql);

    $data = $this->dumpUser($user);

    unset($data[':id']);

    try {
      $stmt->execute($data);

      $user->setId($db->lastInsertId());

      return $user;
    } catch (Exception $e) {
      print_r($e);
    }
  }

  private function update($user) {
    $db = Database::singleton();

    $sql = "UPDATE " . self::_table . " SET name = :name, email = :email, "
        . "password_hash = :password_hash, phone = :phone, "
        . "notify_sales = :notify_promotions, newsletter = :newsletter, "
        . "cpf = :cpf, active = :active, staff = :staff WHERE id = :id";

    $stmt = $db->prepare($sql);
    try {
      $db->beginTransaction();
      $stmt->execute($this->dumpUser($user));
      $db->commit();

      return $user;
    } catch (Exception $e) {
      $db->rollBack();
      print_r($e);
    }
  }

  private function loadUser($row) {
    $user = new User();
    $user->setId($row["id"]);
    $user->setName($row["name"]);
    $user->setEmail($row["email"]);
    $user->setPasswordHash($row["password_hash"]);
    $user->setPhone($row['phone']);
    $user->setNotifyPromotions($row['notify_sales']);
    $user->setNewsletter($row['newsletter']);
    $user->setCpf($row['cpf']);
    $user->setActive($row['active']);
    $user->setStaff($row['staff']);

    return $user;
  }

  private function dumpUser($user) {
    return array(
      ":id" => $user->getId(),
      ":name" => $user->getName(),
      ":email" => $user->getEmail(),
      ":password_hash" => $user->getPasswordHash(),
      ":phone" => $user->getPhone(),
      ":notify_promotions" => $user->getNotifyPromotions() ? 'true' : 'false',
      ":newsletter" => $user->getNewsletter() ? 'true' : 'false',
      ":cpf" => $user->getCpf(),
      ":active" => $user->isActive() ? 'true' : 'false',
      ":staff" => $user->isStaff() ? 'true' : 'false'
    );
  }

  public function remove($user) {
    $db = Database::singleton();
    $sql = "DELETE FROM " . self::_table . " WHERE id = :id";
    $stmt = $db->prepare($sql);

    try {
      $db->beginTransaction();

      $stmt->execute(array());
      $db->commit();
    } catch (Exception $e) {
      $db->rollBack();
      print_r($e);
    }
  }
}
