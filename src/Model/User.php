<?
 class User
 {
 	private $id;
 	private $name;
 	private $email;
 	private $password_hash;
	private $phone;
  private $notify_promotions;
  private $newsletter;
  private $cpf;
  private $active;
  private $staff;

  public function __construct() {
    $this->active = 0;
  }

 	public function setId($id) { $this->id = $id; }
 	public function getId(){ return $this->id; }

 	public function setName($name){ $this->name = $name; }
 	public function getName() { return $this->name; }

 	public function setEmail ($email) { $this->email = $email; }
 	public function getEmail(){ return $this->email; }

 	public function setPasswordHash($password_hash) {
    $this->password_hash = $password_hash;
  }
 	public function getPasswordHash() {
    return $this->password_hash;
  }

	public function setPhone($phone) { $this->phone = $phone; }
 	public function getPhone() { return $this->phone; }

  public function setNotifyPromotions($notify_promotions) {
    $this->notify_promotions = $notify_promotions;
  }
 	public function getNotifyPromotions() { return $this->notify_promotions; }

  public function setNewsletter($newsletter) {
    $this->newsletter = $newsletter;
  }
  public function getNewsletter() { return $this->newsletter; }

  public function setCpf($cpf) { $this->cpf = $cpf; }
  public function getCpf() { return $this->cpf; }

  public function setActive($active) {
    $this->active = $active;
  }

  public function isActive() {
    return $this->active;
  }

  public function setPassword($password) {
    $this->password_hash = password_hash($password, PASSWORD_ARGON2I);
  }

  public function isPassword($password) {
    return password_verify($password, $this->password_hash);
  }

  public function shouldRehash() {
    return password_needs_rehash($this->password_hash, PASSWORD_ARGON2I);
  }

  public function getActive() { return $this->active; }

  public function setStaff($staff) { $this->staff = $staff; }

  public function isStaff() { return $this->staff; }
}
