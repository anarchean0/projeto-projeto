<?
 class Checkout
 {
 	private $id;

 	private $date;

  private $total;

  private $pagamento;

  private $user_id;

 	public function getId() { return $this->id; }

 	public function setId($id) { $this->id = $id; }

 	public function getDate() { return $this->date; }

 	public function setDate($date) { $this->date =  $date; }

  public function getTotal() { return $this->total; }

 	public function setTotal($total) { $this->total =  $total; }

  public function getPagamento() { return $this->pagamento; }

 	public function setPagamento($pagamento) { $this->pagamento =  $pagamento; }

  public function getUserId() { return $this->user_id; }

 	public function setUserId($user_id) { $this->user_id =  $user_id; }
 }
