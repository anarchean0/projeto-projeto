<?php

class UserLoginManager {
  private $user;
  private $userDao;
  private $readSession;

  private static $instance;

  private function __construct() {
    $this->user = null;
    $this->userDao = new UserDao();
    $this->readSession = false;
  }

  public static function getInstance() {
    if (!self::$instance) {
      self::$instance = new UserLoginManager();
    }

    return self::$instance;
  }

  public function isLoggedIn() {
    $this->loadUser();

    if ($this->user) {
      return true;
    }

    return false;
  }

  public function loadUser() {
    if (!$this->readSession) {
      if (array_key_exists('user', $_SESSION)) {
        $this->user = $this->userDao->getUserById($_SESSION['user']);

        if (!$this->user) {
          unset($_SESSION['user']);
        }
      }

      $this->readSession = true;
    }
  }

  public function getUser() {
    return $this->user;
  }

  public function setUser($user) {
    $_SESSION['user'] = $user->getId();
  }

  public function unsetUser() {
    unset($_SESSION['user']);
  }
}
