<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/PHPMailer/src/PHPMailer.php";
require_once "vendor/PHPMailer/src/Exception.php";
require_once "vendor/PHPMailer/src/SMTP.php";

class Mailer {
  private $host;
  private $port;
  private $user;
  private $password;
  private $tls;

  public function __construct() {
    $this->host = APPLICATION_CONFIG["email"]["host"];
    $this->port = APPLICATION_CONFIG["email"]["port"];
    $this->user = APPLICATION_CONFIG["email"]["user"];
    $this->password = APPLICATION_CONFIG["email"]["password"];
    $this->tls = APPLICATION_CONFIG["email"]["tls"];
  }

  public function sendEmail($to, $subject, $message) {
    $mail = new PHPMailer(true);

    if (!is_array($to)) {
      $to = array($to);
    }

    try {
      $mail->isSMTP();
      $mail->Host = $this->host;
      $mail->Port = $this->port;
      $mail->SMTPSecure = $this->tls ? 'tls' : false;

      if ($this->password) {
        $mail->SMTPAuth = true;
        $mail->Username = $this->user;
        $mail->Password = $this->password;
      }

      $mail->setFrom($this->user, "Projeto Projeto");
      call_user_func_array(array($mail, 'addAddress'), $to);

      $mail->isHTML(true);
      $mail->Subject = $subject;
      $mail->Body = $message;
      $mail->AltBody = $message;
      $mail->send();
    } catch (Exception $e) {
      echo "ERROR SENDING EMAIL" . $mail->ErrorInfo;
    }
  }
}
