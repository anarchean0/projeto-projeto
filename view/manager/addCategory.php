
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Categoria</h3>
			<h4 class="">Adicionar nova Categoria</h4>
		</div>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<!-- BEGIN: PAGE CONTENT -->
		<div class="c-content-box c-size-lg">
	<div class="container">

		<form role="form" class="c-shop-form-1" action="index.php?controller=Manager&action=addCategory" method="POST" enctype="multipart/form-data">
			<div class="row">
				<!-- BEGIN: ADDRESS FORM -->
				<div class="col-md-7 c-padding-20">
					<!-- BEGIN: BILLING ADDRESS -->
					<h3 class="c-font-bold c-font-uppercase c-font-24">Categoria</h3>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<label class="control-label">Nome</label>
									<input name="name" id="name" type="text" class="form-control c-square c-theme" placeholder="Informe o nome da Categoria">
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="form-group col-md-12" role="group">
							<button type="submit" name="save" id="save" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Adicionar </button>
							<a href="javascript:history.back()" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancelar</a>
						</div>
					</div>

				</div>
				<!-- END: ADDRESS FORM -->
				<!-- BEGIN: ORDER FORM -->
				</div>
				<!-- END: ORDER FORM -->
			</div>
		</form>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	            crossorigin="anonymous"></script>
							<!-- Adicionando Javascript -->
