<div class="container">
  <h2>Login</h2>
  <form method="POST" action="/?controller=User&action=login">
    <?php if ($errors) { ?>
      <div class="alert alert-danger">
        <ul>
          <?php foreach ($errors as $key => $value) { ?>
            <li><?= $value ?></li>
          <?php } ?>
        </ul>
      </div>
    <?php } ?>

    <div class="form-group">
      <label for="input-email" class="hide">Email</label>
      <input type="email" class="form-control c-square" id="input-email"
             name="email" placeholder="Email" />
    </div>

    <div class="form-group">
      <label for="input-password" class="hide">Senha</label>
      <input type="password" class="form-control c-square" id="input-password"
             name="password" placeholder="Password" />
    </div>

    <div class="form-group">
      <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">
        Login
      </button>
    </div>

    <p>
      <a href="/?controller=User&action=resetPassword">
        Esqueceu a senha?
      </a> |
      <a href="/?controller=User&action=register">
        Não tem cadastro ainda? Se registre aqui!
      </a>
    </p>
  </form>
</div>
