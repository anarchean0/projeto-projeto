<div class="container">
  <h2>Parabéns</h2>
  <p>Seu cadastro foi realizado com sucesso, mas antes de começar a usar o site
    você precisa ativá-lo. Nós enviamos um e-mail para <?= $user->getEmail() ?>,
    você deverá recebê-lo em breve
  </p>
</div>
