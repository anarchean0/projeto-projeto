<div class="container">
  <h2>Resetar Senha</h2>
  <p>Para recuperar sua senha, preencha seu email abaixo</p>
  <form method="POST" action="/?controller=User&action=resetPassword">
    <div class="form-group">
      <label for="input-email" class="hide">Email</label>
      <input type="email" class="form-control c-square" id="input-email"
             name="email" placeholder="Email" />
    </div>

    <div class="form-group">
      <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">
        Recuperar Senha
      </button>
    </div>
  </form>
</div>
