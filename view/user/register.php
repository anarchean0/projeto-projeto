<div class="container">
  <form method="POST" action="/?controller=User&action=register">
    <h3 class="">Cadastro</h3>
    <p>Preencha as informações abaixo para se cadastrar</p>
    <div class="form-group">
      <label for="input-name" class="hide">Nome</label>
      <input type="text" class="form-control c-square" name="name"
          id="input-name" placeholder="Nome" />
    </div>

    <div class="form-group">
      <label for="input-email" class="hide">Email</label>
      <input type="email" class="form-control c-square" name="email"
          id="input-email" placeholder="Email" />
    </div>

    <div class="form-group">
      <label for="input-confirm-email" class="hide">Confirmar email</label>
      <input type="email" class="form-control c-square" name="confirm-email"
          id="input-confirm-email" placeholder="Confirmar email" />
    </div>

    <div class="form-group">
      <label for="input-password" class="hide">Senha</label>
      <input type="password" class="form-control c-square" name="password"
          id="input-password" placeholder="Senha" />
    </div>

    <div class="form-group">
      <label for="input-confirm-password" class="hide">Confirmar a senha</label>
      <input type="password" class="form-control c-square" name="confirm-password"
          id="input-confirm-password" placeholder="Confirmar a senha" />
    </div>

    <div class="form-group">
      <label for="input-cpf" class="hide">CPF</label>
      <input type="text" class="form-control c-square" name="cpf"
          id="input-cpf" placeholder="CPF (somente números 00000000000)" maxlength="11" />
    </div>

    <div class="form-group">
      <label for="input-phone" class="hide">Telefone</label>
      <input type="text" class="form-control c-square" name="phone"
          id="input-phone" placeholder="Telefone +5582991022222" maxlength="14" />
    </div>

    <div class="form-group">
      <div class="c-checkbox">
        <input type="checkbox" class="c-check" name="notify-promotions"
               id="ck-notify" checked="checked" />
        <label for="ck-notify">
          <span></span>
          <span class="check"></span>
          <span class="box"></span>
          Receber promoções por email?
        </label>
      </div>
    </div>

    <div class="form-group">
      <div class="c-checkbox">
        <input type="checkbox" class="c-check" name="newsletter"
               id="ck-newsletter" checked="checked" />
        <label for="ck-newsletter">
          <span></span>
          <span class="check"></span>
          <span class="box"></span>
          Receber novidades por email?
        </label>
      </div>
    </div>

    <fieldset>
      <legend>Endereço</legend>

      <div class="form-group">
        <label for="input-addr-cep" class="hide">CEP</label>
        <input type="text" class="form-control c-square" id="input-addr-cep"
              name="address-cep" placeholder="CEP (12345123)" />
      </div>

      <div class="form-group">
        <label for="input-addr-street" class="hide">Rua</label>
        <input type="text" class="form-control c-square" id="input-addr-street"
               name="address-street" placeholder="Rua" />
      </div>

      <div class="form-group">
        <label for="input-addr-number" class="hide">Número</label>
        <input type="text" class="form-control c-square" id="input-addr-number"
               name="address-number" placeholder="Número" />
      </div>

      <div class="form-group">
        <label for="input-addr-district" class="hide">Bairro</label>
        <input type="text" class="form-control c-square" id="input-addr-district"
               name="address-district" placeholder="Bairro" />
      </div>

      <div class="form-group">
        <label for="input-addr-city" class="hide">Cidade</label>
        <input type="text" class="form-control c-square" id="input-addr-city"
               name="address-city" placeholder="Cidade" />
      </div>

      <div class="form-group">
        <label for="input-addr-state" class="hide">Estado</label>
        <input type="text" class="form-control c-square" id="input-addr-state"
               name="address-state" placeholder="Estado (ex. SP)" />
      </div>
    </fieldset>

    <div class="form-group">
      <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">
        Registrar
      </button>
    </div>
  </form>
</div>

<script>
  (function () {
    var cepField = $("#input-addr-cep");
    var streetField = $("#input-addr-street");
    var districtField = $("#input-addr-district");
    var cityField = $("#input-addr-city");
    var stateField = $("#input-addr-state");

    var addr = [cepField, streetField, districtField, cityField, stateField];

    cepField.on('change', function () {
      var cep = cepField.val();

      if (cep.trim().length == 8) {
        $(addr).prop('disabled', true);
        $.get("https://viacep.com.br/ws/" + cep + "/json/")
          .done(function (info) {
            streetField.val(info.logradouro);
            districtField.val(info.bairro);
            cityField.val(info.localidade);
            stateField.val(info.uf);
          })
          .fail(function () { alert("CEP INVÁLIDO!!!!!!!!!"); })
          .always(function () {
            $(addr).prop('disabled', false);
          });
      }
    });
  })();
</script>
