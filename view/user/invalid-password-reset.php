<div class="container">
  <h2>Oops</h2>
  <p>
    Seus dados de recuperação de senha parecem estar errados ou expirados, se
    quiser pode pedir um novo email em
    <a href="/?controller=User&action=resetPassword">Recuperar Senha</a>
  </p>
</div>
