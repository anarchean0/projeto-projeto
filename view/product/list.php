<label class="control-label c-font-uppercase c-font-bold">Pesquisa</label>
<br>
<form class="c-shop-form-1" action="index.php?controller=Product&action=search" method="post" enctype="multipart/form-data">
  <ul class="c-shop-filter-search-1 list-unstyled">
	<li>
		<h1>Por categoria:</h1>
		<select name="select" id="select" class="form-control">

      <?php
      $categoryDao = new CategoryDao();
      $id = 1;
      foreach($categories as $key => $category) : ?>

    	<option value="<?=$id?>"><?=$categoryDao->getCategory($id)?></option>
      <?$id=$id+1?>

      <?

      endforeach;

      ?>
      <option value="-1">Todas</option>

		</select>
	</li>
  <div class="row">
    <div class="form-group col-md-2">
      <h1>Por nome:</h1>
      <input name="name" id="name" type="text" class="form-control c-square c-theme" placeholder="">
    </div>

    <div class="form-group col-md-10" role="group">
      <h5> <br> </h5>
      <button type="submit" name="search" id="search" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"> Pesquisar </button>
    </div>
  </div>

</ul>
			</div>
			<div class="c-layout-sidebar-content ">
</form>

<div class="c-margin-t-30"></div>

<div class="c-margin-t-20"></div>

<!-- Fazer o for com todos os itens retornados -->
<?php

foreach($products as $key => $product) :
  if($product->getQuatity() >= 5) :?>

<div class="row c-margin-b-40">
	<div class="c-content-product-2 c-bg-white">
		<div class="col-md-4">
			<div class="c-content-overlay">
				<div class="c-overlay-wrapper">
					<div class="c-overlay-content">
						<a href="?controller=ItemStore&action=view&id=<?= $product->getId() ?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
					</div>
				</div>
				<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(<?=$product->getPicture() ?>);"></div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="c-info-list">
				<h3 class="c-title c-font-bold c-font-22 c-font-dark">
					<a class="c-theme-link" href="shop-product-details.html"><?=$product->getName() ?></a>
				</h3>
				<p class="c-desc c-font-16 c-font-thin"><?=$product->getDescription() ?></p>
				<p class="c-price c-font-26 c-font-thin"><?=$product->getPrice() ?></p>
			</div>
			<div>
        <div class="btn-group c-border-left c-border-top" role="group">
          <a href="?controller=ItemStore&action=add&id=<?= $product->getId() ?>" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Comprar</a>
        </div>


			</div>
		</div>
	</div>
</div>

<?
  endif;
endforeach; ?>
