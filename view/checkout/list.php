<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Carrinho de compras</h3>
			<h4 class="">Visualize seus produtos</h4>
		</div>

	</div>
</div>
<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: CONTENT/SHOPS/SHOP-CART-1 -->
		<!-- Obtendo itens do carrinho salvos na sessao -->
		<?
			ShoppingCart::load();
			ShoppingCart::get();

			$cartItems = ShoppingCart::get();
			//Variavel para exibir o sub total de produtos
			$subTotal = 0;

			/*Caso nao haja itens no carrinho sera exbibida uma mensagem
			informado que o carrinho esta vazio e exibido um botao para o usuario ir
			a tela Inicial.
			Caso contrario os itens contidos no carrinho serao exibidos.*/
			$show='';
			$showEmpty='';
			if(empty($cartItems)){
				$show = 'hidden';
				$showEmpty='';
			}else{
				$show='';
				$showEmpty='hidden';
			}


			$taxaEntrega='0';
			if(isset($valorFrete)){
					//Obtendo o valor do frete atraves do xml
					$taxaEntrega = $valorFrete->Valor;
			}

		?>
		<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space c-bg-grey-1" <?= $showEmpty?>>
			<div class="container">
				<div class="c-content-title-4">
					<h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Seu carrinho está vazio</span></h3>
					<div class="c-body c-center">
						<a href="index.php?controller=ItemStore&action=list" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r c-content-v-center">Ir às compras</a>
					</div>
				</div>
			</div>
		</div>
<div class="c-content-box c-size-lg" <?=$show?>>
	<div class="container">
		<div class="c-shop-cart-page-1">
			<div class="row c-cart-table-title">
				<div class="col-md-2 c-cart-image">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Imagem</h3>
				</div>
				<div class="col-md-4 c-cart-desc">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Produto</h3>
				</div>
				<div class="col-md-2 c-cart-qty">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Quantidade</h3>
				</div>
				<div class="col-md-2 c-cart-price">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Preço unitário</h3>
				</div>
				<div class="col-md-1 c-cart-total">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Total</h3>
				</div>
				<div class="col-md-1 c-cart-remove"></div>
			</div>
			<!-- BEGIN: SHOPPING CART ITEM ROW -->
			<?php foreach($cartItems as $cartItem) :?>

			<div class="row c-cart-table-row">
				<h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first"></h2>
				<div class="col-md-2 col-sm-3 col-xs-5 c-cart-image">
					<img src="<?= $cartItem['picture'] ?>"/>
				</div>
				<div class="col-md-4 col-sm-9 col-xs-7 c-cart-desc">
					<h3><a href="index.php?controller=ItemStore&action=view&id=<?=$cartItem['id']?>" class="c-font-bold c-theme-link c-font-22 c-font-dark"><?= $cartItem['name'] ?></a></h3>
					<!-- <p>Color: Blue</p>
					<p>Size: S</p> -->
				</div>

				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-qty">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">QNT</p>
					<div class="c-input-group c-spinner">

					    <div class="c-input-group-btn-horizontal">
					    	<a href="index.php?controller=ShoppingCart&action=add&id=<?=$cartItem['id']?>&quantity=<?= $cartItem['quantity']-1 ?>" class="btn btn-default" type="button" data_input="c-item-1"><i class="fa fa-minus"></i></a>
								<input type="text" class="form-control c-item-1" value="<?= $cartItem['quantity'] ?>">
								<a href="index.php?controller=ShoppingCart&action=add&id=<?=$cartItem['id']?>&quantity=<?= $cartItem['quantity']+1 ?>"  class="btn btn-default" type="button" data_input="c-item-1" data-maximum="10"><i class="fa fa-plus"></i></a>
					    </div>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 c-cart-price">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Preço Unitário</p>
					<p class="c-cart-price c-font-bold">R$<?=$cartItem['price'] ?></p>
				</div>
				<div class="col-md-1 col-sm-3 col-xs-6 c-cart-total">
					<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Total</p>
					<? $totalProduct =  $cartItem['price'] * $cartItem['quantity'];
						 $subTotal+= $totalProduct;

					?>
					<p class="c-cart-price c-font-bold">R$<?=$totalProduct?></p>
				</div>
				<div class="col-md-1 col-sm-12 c-cart-remove">
					<a href="index.php?controller=ShoppingCart&action=delete&id=<?=$cartItem['id']?>" class="c-theme-link c-cart-remove-desktop">×</a>
					<a href="#" class="c-cart-remove-mobile btn c-btn c-btn-md c-btn-square c-btn-red c-btn-border-1x c-font-uppercase">Remove item from Cart</a>
				</div>
			</div>
			<? endforeach; ?>
			<!-- END: SHOPPING CART ITEM ROW -->

			<!-- BEGIN: CEP ITEM ROW -->
			<div class="row">
				<form class="form-group col-md-8 col-md-offset-9" action="index.php?controller=ShoppingCart&action=frete" method="POST" enctype="multipart/form-data" <?=$show?> >
						<div class="col-md-4">
								<label class="control-label">Calcular frete</label>
								<input type="text" class="form-control c-square c-theme" placeholder="Informe seu CEP" id="cep" name="cep">
								<button type="submit" name="frete" id="frete" class="btn btn-default c-btn-uppercase c-font-bold c-font-uppercase c-cart-float-r">Calcular frete</button>
							</div>
					</form>
			</div>
			<script>

			</script>
			<!-- END: CEP ITEM ROW -->
			<!-- BEGIN: SUBTOTAL ITEM ROW -->
			<div class="row" <?=$show?>>
				<div class="c-cart-subtotal-row c-margin-t-20">
					<div class="col-md-2 col-md-offset-9 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Subtotal</h3>
					</div>
					<div class="col-md-1 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-bold c-font-16">R$<?=$subTotal ?></h3>
					</div>
				</div>
			</div>
			<!-- END: SUBTOTAL ITEM ROW -->
			<!-- BEGIN: SUBTOTAL ITEM ROW -->
			<div class="row" <?=$show?>>
				<div class="c-cart-subtotal-row">
					<div class="col-md-2 col-md-offset-9 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Taxa de entrega</h3>
					</div>
					<div class="col-md-1 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-bold c-font-16">R$<?= $taxaEntrega ?></h3>
					</div>
				</div>
			</div>
			<!-- END: SUBTOTAL ITEM ROW -->
			<!-- BEGIN: SUBTOTAL ITEM ROW -->
			<div class="row" <?=$show?>>
				<div class="c-cart-subtotal-row">
					<div class="col-md-2 col-md-offset-9 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Total</h3>
					</div>
					<div class="col-md-1 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-bold c-font-16">R$<?=$subTotal + $taxaEntrega?></h3>
					</div>
				</div>
			</div>
			<!-- END: SUBTOTAL ITEM ROW -->
			<div class="c-cart-buttons" <?=$show?>>
				<a href="javascript:history.back()" class="btn c-btn btn-lg c-btn-red c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-l">Continuar Comprando</a>
				<!-- Mudar aqui para outros usuarios -->
				<?
				$loginManager = UserLoginManager::getInstance();
				$id = 0;
				if($loginManager->isLoggedIn()){
					$id = $loginManager->getUser();
				}
				?>
				<a href="index.php?controller=Checkout&action=checkout&id=<?= $id?>" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Checkout</a>
				</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-CART-1 -->
