<?
$loginManager = UserLoginManager::getInstance();
?>
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Endereço</h3>
			<h4 class="">Adicionar novo endereço</h4>
		</div>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<!-- BEGIN: PAGE CONTENT -->
		<div class="c-content-box c-size-lg">
	<div class="container">

		<form role="form" class="c-shop-form-1" action="index.php?controller=Address&action=add&id=<?= $loginManager->getUser()?>" method="POST" enctype="multipart/form-data">
			<div class="row">
				<!-- BEGIN: ADDRESS FORM -->
				<div class="col-md-7 c-padding-20">
					<!-- BEGIN: BILLING ADDRESS -->
					<h3 class="c-font-bold c-font-uppercase c-font-24">Endereço de entrega</h3>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<label class="control-label">CEP</label>
									<input name="cep" id="cep" type="text" class="form-control c-square c-theme" placeholder="Informe seu CEP">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Endereço</label>
							<input name="street" id="street" type="text" class="form-control c-square c-theme" placeholder="Informe seu endereço">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Cidade</label>
							<input name="city" id="city" type="text" class="form-control c-square c-theme" placeholder="Informe sua cidade">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<label class="control-label">Estado</label>
							<input name="state" id="state" type="text" class="form-control c-square c-theme" placeholder="Informe seu estado">
						</div>
						<div class="form-group col-md-6">
								<label class="control-label">Bairro</label>
							<input name="district" id="district" type="text" class="form-control c-square c-theme" placeholder="Informe seu bairro">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
								<label class="control-label">Numero</label>
							<input name="number" id="number" type="text" class="form-control c-square c-theme" placeholder="Informe seu numero">
						</div>
				</div>

					<div class="row">
						<div class="form-group col-md-12" role="group">
							<button type="submit" name="save" id="save" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Adicionar </button>
							<a href="javascript:history.back()" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancelar</a>
						</div>
					</div>

				</div>
				<!-- END: ADDRESS FORM -->
				<!-- BEGIN: ORDER FORM -->
				</div>
				<!-- END: ORDER FORM -->
			</div>
		</form>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	            crossorigin="anonymous"></script>
							<!-- Adicionando Javascript -->
	 <script type="text/javascript" >

			 $(document).ready(function() {

					 function limpa_formulário_cep() {
							 // Limpa valores do formulário de cep.
							 $("#street").val("");
							 $("#district").val("");
							 $("#city").val("");
							 $("#state").val("");
							 $("#ibge").val("");
					 }

					 //Quando o campo cep perde o foco.
					 $("#cep").blur(function() {

							 //Nova variável "cep" somente com dígitos.
							 var cep = $(this).val().replace(/\D/g, '');

							 //Verifica se campo cep possui valor informado.
							 if (cep != "") {

									 //Expressão regular para validar o CEP.
									 var validacep = /^[0-9]{8}$/;

									 //Valida o formato do CEP.
									 if(validacep.test(cep)) {

											 //Preenche os campos com "..." enquanto consulta webservice.
											 $("#street").val("...");
											 $("#district").val("...");
											 $("#city").val("...");
											 $("#state").val("...");
											 $("#ibge").val("...");

											 //Consulta o webservice viacep.com.br/
											 $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

													 if (!("erro" in dados)) {
															 //Atualiza os campos com os valores da consulta.
															 $("#street").val(dados.logradouro);
															 $("#district").val(dados.bairro);
															 $("#city").val(dados.localidade);
															 $("#state").val(dados.uf);
															 $("#ibge").val(dados.ibge);
													 } //end if.
													 else {
															 //CEP pesquisado não foi encontrado.
															 limpa_formulário_cep();
															 alert("CEP não encontrado.");
													 }
											 });
									 } //end if.
									 else {
											 //cep é inválido.
											 limpa_formulário_cep();
											 alert("Formato de CEP inválido.");
									 }
							 } //end if.
							 else {
									 //cep sem valor, limpa formulário.
									 limpa_formulário_cep();
							 }
					 });
			 });

	 </script>
