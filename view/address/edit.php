<?
$loginManager = UserLoginManager::getInstance();
?>
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Editar endereço</h3>
			<h4 class="">Edite seu endereço</h4>
		</div>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<!-- BEGIN: PAGE CONTENT -->
		<div class="c-content-box c-size-lg">
	<div class="container">
		<form class="c-shop-form-1" action="index.php?controller=Address&action=edit&addressid=<?= $address->getId() ?>&id=<?= $loginManager->getUser()?>" method="post" enctype="multipart/form-data">
			<div class="row">
				<!-- BEGIN: ADDRESS FORM -->
				<div class="col-md-7 c-padding-20">
					<!-- BEGIN: BILLING ADDRESS -->
					<h3 class="c-font-bold c-font-uppercase c-font-24">Endereço de Entrega</h3>

					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">CEP</label>
							<input type="text" value="<?= $address->getCep() ?>" class="form-control c-square c-theme" placeholder="Informe seu Cep" id="cep" name="cep">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Endereço</label>
							<input type="text" value="<?= $address->getStreet() ?>" class="form-control c-square c-theme" placeholder="Informe seu endereço" id="street" name="street">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Cidade</label>
							<input type="text" value="<?= $address->getCity() ?>" class="form-control c-square c-theme" placeholder="Informe sua cidade" id="city" name="city">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Estado</label>
							<input type="text" value="<?= $address->getState() ?>" class="form-control c-square c-theme" placeholder="Informe seu estado" id="state" name="state">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="form-group col-md-6">
									<label class="control-label">Bairro</label>
									<input type="text" value="<?= $address->getDistrict() ?>" class="form-control c-square c-theme" placeholder="Informe seu Bairro" id="district" name="district">
								</div>
								<div class="col-md-6">
									<label class="control-label">Numero</label>
									<input type="text" value="<?= $address->getNumber() ?>" class="form-control c-square c-theme" placeholder="Informe seu numero" id="number" name="number">
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-12" role="group">
							<button type="submit" name="save" id="save" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"> Atualizar </button>
							<a href="javascript:history.back()" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</a>
						</div>
					</div>

				</div>
				<!-- END: ADDRESS FORM -->
				<!-- BEGIN: ORDER FORM -->
				</div>
				<!-- END: ORDER FORM -->
			</div>
		</form>
	</div>
