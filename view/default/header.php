<!DOCTYPE html>
<!--
Theme: JANGO - Ultimate Multipurpose HTML Theme Built With Twitter Bootstrap 3.3.7
Version: 2.0.1
Author: Themehats
Site: http://www.themehats.com
Purchase: http://themeforest.net/item/jango-responsive-multipurpose-html5-template/11987314?ref=themehats
Contact: support@themehats.com
Follow: http://www.twitter.com/themehats
-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br"  >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>
	<title>JANGO LIVRE</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta content="" name="description"/>
	<meta content="" name="author"/>
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
	<link href="assets/plugins/socicon/socicon.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/animate/animate.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- END GLOBAL MANDATORY STYLES -->

			<!-- BEGIN: BASE PLUGINS  -->
			<link href="assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>
			<link href="assets/plugins/revo-slider/css/layers.css" rel="stylesheet" type="text/css"/>
			<link href="assets/plugins/revo-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
			<link href="assets/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>
			<link href="assets/plugins/owl-carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css"/>
			<link href="assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
			<link href="assets/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css"/>
				<!-- END: BASE PLUGINS -->


    <!-- BEGIN THEME STYLES -->
	<link href="assets/demos/default/css/plugins.css" rel="stylesheet" type="text/css"/>
	<link href="assets/demos/default/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="assets/demos/default/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css"/>
	<link href="assets/demos/default/css/custom.css" rel="stylesheet" type="text/css"/>
	<!-- END THEME STYLES -->
	<!-- BEGIN SCRIPTS -->
	<script src="assets/plugins/jquery.min.js" type="text/javascript" ></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
	<!-- END SCRIPTS -->
	<link rel="shortcut icon" href="favicon.ico"/>
</head><body class="c-layout-header-fixed c-layout-header-mobile-fixed c-shop-demo-1">

	<!-- BEGIN: LAYOUT/HEADERS/HEADER-SHOP-1 -->
<!-- BEGIN: SHOP HEADER 1 -->
<header class="c-layout-header c-layout-header-6 c-layout-header-default-mobile c-layout-header-shop-1" data-minimize-offset="80">
	<div class="c-topbar">
		<div class="c-shop-topbar-offer c-theme-bg">
			<div class="container">
				<h3 class="c-shop-topbar-offer-title c-font-white c-font-uppercase c-font-14 c-font-thin c-center">
					O gerente ficou maluco. Descontos de até 20%!
					<a href="#" class="c-font-bold c-font-dark c-font-white-hover">Compre Já!</a>
				</h3>
				<a href="#" class="c-shop-topbar-close c-font-dark c-font-white-hover"><i class="fa fa-close"></i></a>
			</div>
		</div>
		<div class="container">
			<nav class="c-top-menu">
				<ul class="c-links c-theme-ul">
				<?php
					$loginManager = UserLoginManager::getInstance();

					if (!$loginManager->isLoggedIn()) {
				?>
					<li><a href="index.php?controller=User&action=login" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Entrar</a></li>
					<li><a href="index.php?controller=User&action=register" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Cadastro</a></li>
				<?php } else { ?>
					<li>Bem vindo, <?= $loginManager->getUser()->getName() ?></li>
					<li><a href="index.php?controller=User&action=logout" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Logout</a></li>
				<?php } ?>
				</ul>
				<ul class="c-ext hide c-theme-ul">
					<li class="c-lang dropdown c-last">
						<a href="#">en</a>
						<ul class="dropdown-menu pull-right" role="menu">
							<li class="active"><a href="#">English</a></li>
							<li><a href="#">German</a></li>
							<li><a href="#">Espaniol</a></li>
							<li><a href="#">Portugise</a></li>
						</ul>
					</li>
					<li class="c-search hide">
						<!-- BEGIN: QUICK SEARCH -->
						<form action="#">
							<input type="text" name="query" placeholder="search..." value="" class="form-control" autocomplete="off">
							<i class="fa fa-search"></i>
						</form>
						<!-- END: QUICK SEARCH -->
					</li>
				</ul>
			</nav>
			<div class="c-brand">
				<a href="index.php" class="c-logo">
					<img src="assets/base/img/layout/logos/logo-3.png" alt="JANGO" class="c-desktop-logo">
					<img src="assets/base/img/layout/logos/logo-3.png" alt="JANGO" class="c-desktop-logo-inverse">
					<img src="assets/base/img/layout/logos/logo-3.png" alt="JANGO" class="c-mobile-logo">
				</a>
				<button class="c-topbar-toggler" type="button">
					<i class="fa fa-ellipsis-v"></i>
				</button>
				<button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
					<span class="c-line"></span>
					<span class="c-line"></span>
					<span class="c-line"></span>
				</button>
				<button class="c-search-toggler" type="button">
					<i class="fa fa-search"></i>
				</button>
				<button class="c-cart-toggler" type="button">
					<i class="icon-handbag"></i> <span class="c-cart-number c-theme-bg">2</span>
				</button>
			</div>
		</div>
	</div>
	<div class="c-navbar">
		<div class="container">
			<!-- BEGIN: BRAND -->
			<div class="c-navbar-wrapper clearfix">
				<!-- END: BRAND -->
				<!-- BEGIN: QUICK SEARCH -->
				<form class="c-quick-search" action="#">
					<input type="text" name="query" placeholder="Procurar..." value="" class="form-control" autocomplete="off">
					<span class="c-theme-link">&times;</span>
				</form>
				<!-- END: QUICK SEARCH -->
				<!-- BEGIN: HOR NAV -->
				<!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU-SHOP -->
<!-- BEGIN: SHOP MEGA MENU -->
<!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
<nav class="c-mega-menu c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
	<ul class="nav navbar-nav c-theme-nav">
	    <li class="c-active">
	        <a href="index.php" class="c-link dropdown-toggle">Página Inicial
	            <span class="c-arrow c-toggler"></span>
	        </a>
	    </li>
	    <li>
	        <a href="javascript:;" class="c-link dropdown-toggle">Categorias
	            <span class="c-arrow c-toggler"></span>
	        </a>
	        <div class="dropdown-menu c-menu-type-mega c-menu-type-fullwidth" style="min-width: auto">
	            <div class="row">
	                <div class="col-md-7">
	                	<div class="row">
	                		<div class="col-md-6">
												<?php
												$categoryDao = new CategoryDao();
												$categories = $categoryDao->getAll();
												foreach($categories as $category) {
												?>
	                			<ul class="dropdown-menu c-menu-type-inline">
	                				<li>
	                					<h3><a href="index.php?controller=Product&action=search&select=<?= $category->getId()?>&name"><?=$category->getName()?></a></h3>
	                				</li>
	                    		</ul>
												<?php } ?>
	                		</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </li>

	    <li>
					<?
					$id = 0;
					if($loginManager->isLoggedIn())
					{
						$id = $loginManager->getUser();
					}
					?>
	        <a href="index.php?controller=Manager&action=list&id=<?= $id ?>" class="c-link dropdown-toggle">Gerenciar
	            <span class="c-arrow c-toggler"></span>
	        </a>
	    </li>
	    <li>
	        <a href="javascript:;" class="c-link dropdown-toggle">Sobre / Contato
	            <span class="c-arrow c-toggler"></span>
	        </a>
	    </li>
	</ul>
	<ul class="nav navbar-nav c-theme-nav c-float-right">
		<li class="c-cart-toggler-wrapper">
			<!-- icone do carrinho-->
	        <a href="index.php?controller=Checkout&action=list" class="c-btn-icon c-cart-toggler">
	            <i class="icon-handbag c-cart-icon"></i>
	        </a>
	    </li>
	    <li class="c-search-toggler-wrapper">
			<a href="index.php?controller=Product&action=list" class="c-btn-icon"><i class="fa fa-search"></i></a>
		</li>

	</ul>
</nav>
<!-- END: MEGA MENU -->
<!-- END: LAYOUT/HEADERS/MEGA-MENU-SHOP -->
				<!-- END: HOR NAV -->
			</div>
			<!-- BEGIN: LAYOUT/HEADERS/QUICK-CART -->

		</div>
	</div>
</header>
<!-- END: SHOP HEADER 1 -->

<!-- END: LAYOUT/HEADERS/HEADER-SHOP-1 -->

	<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>
		</button>
	</div>
	<div class="c-content">
		<div class="c-section">
			<h3>JANGO DEMOS</h3>
			<div class="c-settings c-demos c-bs-grid-reset-space">
				<div class="row">
					<div class="col-md-12">
						<a href="../default/index.html" class="c-demo-container c-demo-img-lg">
							<div class="c-demo-thumb active">
								<img src="assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="../corporate_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-left">
								<img src="assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
					<div class="col-md-6">
						<a href="../agency_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-right">
								<img src="assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="c-section">
			<h3>Theme Colors</h3>
			<div class="c-settings">

				<span class="c-color c-default c-active" data-color="default"></span>

				<span class="c-color c-green1" data-color="green1"></span>
				<span class="c-color c-green2" data-color="green2"></span>
				<span class="c-color c-green3" data-color="green3"></span>

				<span class="c-color c-yellow1" data-color="yellow1"></span>
				<span class="c-color c-yellow2" data-color="yellow2"></span>
				<span class="c-color c-yellow3" data-color="yellow3"></span>

				<span class="c-color c-red1" data-color="red1"></span>
				<span class="c-color c-red2" data-color="red2"></span>
				<span class="c-color c-red3" data-color="red3"></span>

				<span class="c-color c-purple1" data-color="purple1"></span>
				<span class="c-color c-purple2" data-color="purple2"></span>
				<span class="c-color c-purple3" data-color="purple3"></span>

				<span class="c-color c-blue1" data-color="blue1"></span>
				<span class="c-color c-blue2" data-color="blue2"></span>
				<span class="c-color c-blue3" data-color="blue3"></span>

				<span class="c-color c-brown1" data-color="brown1"></span>
				<span class="c-color c-brown2" data-color="brown2"></span>
				<span class="c-color c-brown3" data-color="brown3"></span>

				<span class="c-color c-dark1" data-color="dark1"></span>
				<span class="c-color c-dark2" data-color="dark2"></span>
				<span class="c-color c-dark3" data-color="dark3"></span>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Type</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Mode</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Mega Menu Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Font Style</h3>
			<div class="c-settings">
				<input type="button" class="c-settina href="index.php?controller=ItemStore&action=list" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r c-content-v-center">Ir às compras</a>
					</div>g_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Reading Style</h3>
			<div class="c-settings">
				<a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">LTR</a>
				<a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">RTL</a>
			</div>
		</div>
	</div>
</nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: PAGE CONTENT -->
