
CREATE TABLE checkout
(
  id serial primary key,
  cdate date not null,
  total numeric not null,
  pagamento character varying(100) not null,
	user_id integer not null,
  foreign key (user_id) references users(id)
);
