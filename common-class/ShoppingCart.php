<?php

class ShoppingCart
{
	private static $itens = array();

	public static function get()
	{
		return self::$itens;
	}

	public static function load()
	{
		if(isset($_SESSION['SHOPPING_CART']))
			self::$itens = unserialize($_SESSION['SHOPPING_CART']);
	}

	public static function save ()
	{
		$_SESSION['SHOPPING_CART'] = serialize(self::$itens);

	}

	public static function add ($itemStore, $quantity)
	{

		self::$itens[$itemStore->getId()] = array(
				"id" => $itemStore->getId(),
				"name" => $itemStore->getName(),
				"description" => $itemStore->getDescription(),
				"price" => $itemStore->getPrice(),
				"picture" => $itemStore->getPicture(),
				"quantity" => $quantity

		);

	}

	public static function delete ($id){
				unset(self::$itens[$id]);

	}

	public static function clear()
	{
		unset($_SESSION['SHOPPING_CART']);
	}
}
