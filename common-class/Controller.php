<?php

class Controller
{
	private $model;

	private $view = false;
	
	private $route;

	public function __contruct(){}
	
	public function showView($viewModel = false)
	{
		Renderer::render($viewModel, $this->getRoute());
	}
	public function getView()
	{
		return $this->view;
	}

	public function setRoute($route)
	{
		$this->route = $route; 
	}
	public function getRoute()
	{
		return $this->route;
	}
}