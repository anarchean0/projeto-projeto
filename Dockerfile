FROM php:apache

RUN apt-get update && apt-get install -y \
      libfreetype6-dev libjpeg62-turbo-dev libpng-dev libpq-dev \
    && docker-php-ext-install -j$(nproc) iconv pdo pdo_pgsql \
    && docker-php-ext-configure gd --with-freetyped-dir=/usr/include --with-jpeg-dir=/usr/include \
    && docker-php-ext-install -j$(nproc) gd
